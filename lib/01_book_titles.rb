class Book
  attr_accessor :title

  def title
    lowercase = %w[and to the of in an a]

    arr = @title.split.map.with_index do |word, idx|
      lowercase.include?(word) && idx > 0 ? word : word.capitalize
    end

    arr.join(' ')
  end
end
