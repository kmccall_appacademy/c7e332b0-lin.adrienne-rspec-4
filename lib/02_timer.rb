class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 3600
    mins = (@seconds - (3600 * hours)) / 60
    secs = @seconds - 3600 * hours - 60 * mins

    "#{padded(hours)}:#{padded(mins)}:#{padded(secs)}"
  end

  def padded(num)
    num.to_s.length == 1 ? (num_str = "0#{num}") : (num_str = num.to_s)
    num_str
  end
end
