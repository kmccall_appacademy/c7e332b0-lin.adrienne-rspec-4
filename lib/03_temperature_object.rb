class Temperature

  def initialize(options_hsh)
    @options_hsh = options_hsh
  end

  def in_fahrenheit
    if @options_hsh.key?(:f)
      @options_hsh[:f]
    else
      Temperature.ctof(@options_hsh[:c])
    end
  end

  def in_celsius
    if @options_hsh.key?(:c)
      @options_hsh[:c]
    else
      Temperature.ftoc(@options_hsh[:f])
    end
  end

  def self.ftoc(num)
    (num - 32) * 5/9.0
  end

  def self.ctof(num)
    num * 9.0/5 + 32
  end

  def self.from_celsius(num)
    Temperature.new(:c => num)
  end

  def self.from_fahrenheit(num)
    Temperature.new(:f => num)
  end
end

class Celsius < Temperature
  def initialize(num)
    @options_hsh = { :c => num }
  end
end

class Fahrenheit < Temperature
  def initialize(num)
    @options_hsh = { :f => num }
  end
end
