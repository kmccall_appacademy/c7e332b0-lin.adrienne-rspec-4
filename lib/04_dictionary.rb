class Dictionary

  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(input)
    if input.is_a?(Hash)
      @entries = @entries.merge(input)
    else
      @entries[input] = nil
    end
  end

  def include?(word)
    @entries.keys.include?(word)
  end

  def keywords
    @entries.keys.sort
  end

  def find(word)
    selected = @entries.keys.select { |key| /#{word}/ =~ key }

    hsh = {}
    selected.each { |key| hsh[key] = @entries[key] }

    hsh
  end

  def printable
    keys = @entries.keys.sort
    printed = keys.map do |key|
      "[#{key}] \"#{@entries[key]}\""
    end
    printed.join("\n")
  end

end
